# ESP8266_SERVO_CONTROL

ESP8266 Pinewood Derby track start gate controller

## Hardware Setup

-          Label GPIO  Input          Output                 Notes
- trigger   D0    16   no interrupt   no PWM or I2C support  HIGH at boot, used to wake up from deep sleep
- lane-1    D1     5   OK             OK                     often used as SCL (I2C)
- lane-2    D2     4   OK             OK                     often used as SDA (I2C)
- servo     D3     0   pulled up      OK                     pulled-up, connected to FLASH button, boot fails if pulled LOW: best pin for servo, moves minimal at boot
-           D4     2   pulled up      OK                     HIGH at boot, connected to on-board LED, boot fails if pulled LOW
- gate-sw   D5    14   OK             OK                     SPI (SCLK)
- lane-3    D6    12   OK             OK                     SPI (MISO)
- lane-4    D7    13   OK             OK                     SPI (MOSI)
-           D8    15   pulled to GND  OK                     SPI (CS), boot fails if pulled HIGH
- reserve   RX     3   OK             RX pin                 HIGH at boot
- reserve   TX     1   TX pin         OK                     HIGH at boot, debug output at boot, boot fails if pulled LOW
-           A0  ADC0   Analog Input   X 

## Usage

Change these lines as per yours:  
`
const char *AP_SSID = "REPLACE_WITH_YOUR_SSID";         // specify your Access Point Mode SSID  
const char *AP_PASSWORD = "REPLACE_WITH_YOUR_PASSWORD"; // specify your Access Point Mode Password  
const uint8_t TRIGGER_PIN = D0;                         // specify trigger digital input pin  
const uint8_t SERVO_PIN = D3;                           // specify servo PWM pin  
`

## Acknowledgment

based on:  
https://randomnerdtutorials.com/esp8266-pinout-reference-gpios/  
https://github.com/kumaraditya303/ESP8266_SERVO_CONTROLLER  
https://github.com/esp8266/Arduino/tree/master/libraries/DNSServer/examples/CaptivePortalAdvanced  

## License

MIT License

Copyright (c) 2022 Jacob van Bergeijk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.